class SummationController {
    private SummationModel summationModel;

    SummationController(SummationModel summationModel) {
        this.summationModel = summationModel;
    }

    public void summarize(int counter, int threadFraction) {
        int sum = 0;

        for (int i = 1; i <= threadFraction; i++) {
            sum += counter++;
        }

        synchronized(summationModel) {
            summationModel.addSum(sum);
        }

        System.out.printf("%s: summing is done\n", Thread.currentThread().getName());
//        try {
//            lock.lock();
//            summationModel.setCounter(counter);
//            summationModel.addSum(sum);
//
//        } catch (Exception ex) {
//            System.out.printf("%s: error occurred", Thread.currentThread().getName());
//            System.out.println(ex.getMessage());
//        }
//        finally {
//            lock.unlock();
//        }
    }
}
