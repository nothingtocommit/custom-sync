public class CustomSync {
    public static void main(String[] args) {
        int maxNum = 100;
        int threadNum = 20;

        if (threadNum > maxNum) threadNum = maxNum;

        int threadFraction = maxNum / threadNum;
        int extraIterations = threadNum - (maxNum % threadNum);

        SummationModel summationModel = new SummationModel();
        SummationController summationController = new SummationController(summationModel);

        SummationTask[] tasks = new SummationTask[threadNum];

        boolean isExtraIterNeeded = false;
        int extraIterAdded = 1;

        for (int i = 0; i < threadNum; i++) {

            int counter;
            if (isExtraIterNeeded) {
                counter = i * (threadFraction - 1) + extraIterAdded++;
            } else {
                counter = i * threadFraction;
            }

            if (!isExtraIterNeeded) {
                if (counter == extraIterations * threadFraction) {
                    threadFraction++;
                    isExtraIterNeeded = true;
                }
            }

            tasks[i] = new SummationTask(summationController, counter + 1, threadFraction);
        }

        for (int i = 0; i < threadNum; i++) {
            tasks[i].start();
        }

        for (SummationTask task : tasks) {
                try {
                    synchronized(task) {
                        while (!task.isCompleted()) {
                            System.out.println("wait");
                            task.wait();
                        }
                        task.notify();
                    }
                } catch(InterruptedException ex) {
                    System.out.printf("%s: error occurred", Thread.currentThread().getName());
                    System.out.println(ex.getMessage());
                }
        }
        System.out.println("Total sum: " + summationModel.getTotalSum());
    }
}


