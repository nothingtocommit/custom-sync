class SummationModel {
    private int totalSum = 0;

    public void addSum(int sum) {
        this.totalSum += sum;
    }

    public int getTotalSum() {
        return totalSum;
    }

}
