class SummationTask extends Thread {
    private SummationController summationController;
    private int counter;
    private int threadFraction;
    private boolean isCompleted = false;

    SummationTask(SummationController summationController, int counter, int threadFraction) {
        this.summationController = summationController;
        this.counter = counter;
        this.threadFraction = threadFraction;
    }

    public void run() {
        System.out.printf("%s: ready to summarize\n", Thread.currentThread().getName());

        summationController.summarize(counter, threadFraction);

        isCompleted = true;
    }

    public boolean isCompleted() {
        return isCompleted;
    }
}

